from django import forms

class SearchForm(forms.Form):
    attrs = {
        'type':'query',
        'class': 'form-control',
        'id':'search-query',
        'placeholder':'Search books here!'
    }

    key_input = forms.CharField(required=False,label='', widget=forms.TextInput(attrs=attrs))
