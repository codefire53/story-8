from django.shortcuts import render
from .forms import SearchForm

# Create your views here.
def index(request):
    response = {'search_form':SearchForm}
    return render(request, 'searchpage.html', response)

