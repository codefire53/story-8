from django.test import TestCase,LiveServerTestCase,Client
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time
from django.urls import resolve
import chromedriver_binary
from .views import index
# Create your tests here.
class UnitTest(TestCase):
    def test_url_existence(self):
        response = Client().get("/searchpage/")
        self.assertEqual(response.status_code, 200)
    
    def test_url_used_function(self):
        found = resolve('/searchpage/')
        self.assertEqual(found.func, index)

    def test_url_using_correct_html(self):
        response = Client().get("/searchpage/")
        self.assertTemplateUsed(response, 'searchpage.html')
    
    def test_url_not_exist(self):
        response = Client().get('/BottomText/')
        self.assertEqual(response.status_code, 404)



class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
        #self.selenium=webdriver.Chrome()

    def tearDown(self):
        self.selenium.quit()

    def test_searchbar(self):
        self.selenium.get(self.live_server_url + '/searchpage/')
        time.sleep(3)
        self.assertIn("Sophie's World", self.selenium.page_source)
        form=self.selenium.find_element_by_id('search-query')
        button=self.selenium.find_element_by_id('search-button')
        form.send_keys('homo deus')
        button.click()
        time.sleep(5)
        res = self.selenium.find_element_by_id('books-list')
        self.assertIn('homo deus', res.get_attribute('innerHTML').lower())

